# Noodle is an open-source and modular Discord moderation bot.
# Copyright (C) 2022-2023 Deadspike-san

colors: dict[str, int] = {
    "cyan_light": 0x1abc9c,
    "cyan_dark": 0x11806a,
    "green_light": 0x2ecc71,
    "green_dark": 0x1f8b4c,
    "blue_light": 0x3498db,
    "blue_dark": 0x206694,
    "purple_light": 0x9b59b6,
    "purple_dark": 0x71368a,
    "red_light": 0xe91e63,
    "red_dark": 0xad1457,
    "yellow_light": 0xf1c40f,
    "yellow_dark": 0xc27c0e,
    "gold_light": 0xe67e22,
    "gold_dark": 0xa84300,
    "orange_light": 0xe74c3c,
    "orange_dark": 0x992d22,
    "gray_light": 0x95a5a6,
    "gray_dark": 0x979c9f,
}
