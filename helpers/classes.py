# Noodle is an open-source and modular Discord moderation bot.
# Copyright (C) 2022-2023 Deadspike-san

from abc import ABC
from dataclasses import dataclass


@dataclass
class AbstractDataClass(ABC):

    def __new__(cls, *args, **kwargs):
        if cls == AbstractDataClass or cls.__bases__[0] == AbstractDataClass:
            raise TypeError("Cannot instantiate abstract class.")
        return super().__new__(cls)
