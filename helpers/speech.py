"""Goofy speech modifications to modulate the say and quip commands"""

from typing import Callable
import re


def snekkify(message: str) -> str:
    messsage = re.sub(r"(ss|s)", "sss", str(message))
    return re.sub(r"(Ss|S)", "Sss", messsage)


speech: dict[str, Callable[[str], str]] = {
    "snek": snekkify,
}
