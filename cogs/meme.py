# Noodle is an open-source and modular Discord moderation bot.
# Copyright (C) 2022-2023 Deadspike-san
"""A goofy joke module."""

import random
import re
import requests

from interactions import (
    Channel,
    CommandContext as sctx,
    EmbedAuthor,
    EmbedFooter,
    Extension,
    HTTPClient,
    User,
    extension_command as command,
    extension_listener as listener,
    Member,
    Message,
    option,
)

from app.conf import scope
from app.database import settings_service as service
from app.settings import Setting, Settings
from app.settings import BoolValidator
from helpers.basics import Client, Embed, get_command_list, say
from helpers.files import read_text


class Meme(Extension):
    """A class of goofy joke commands and listeners.

    The swears list is a list of prohibited words for the guild. These naughty
    words will be censored with the names of random Pokemon from the pokemon
    list.
    """

    def __init__(self, bot: Client):
        self.bot: Client = bot
        self.swears: list[str] = read_text("data/swears_full.txt")
        self.pokemon: list[str] = read_text("data/pokemon_names.txt")
        self.settings: Settings = Settings(
            Setting(self.__class__.__name__, "pokeswears", False,
                    BoolValidator(convert=True)),
            Setting(self.__class__.__name__, "pokeswears_blocklist", ()),
            Setting(self.__class__.__name__, "pokeswears_sublist", ()),
            Setting(self.__class__.__name__, "pokeswears_replacedict", {}),
        )

    @command(name="iseven")  # type: ignore
    @option(description="The integer to query", required=True)
    async def is_even(self, ctx: sctx, query: int) -> None:
        """Check if an integer is even using isevenapi.

        Apologies for the ads, the isEven Premium subscription was a little
        pricey.

        Args:
            query: The integer to query.
        """
        ie = None
        message = ""
        try:
            r = requests.get(
                url=f"https://api.isevenapi.xyz/api/iseven/{query}/")
            ie = r.json()
            if ie["iseven"]:
                message = f"{query} is even.\n\n{ie['ad']}"
            else:
                message = f"{query} is not even.\n\n{ie['ad']}"
        except KeyError:
            if ie:
                message = ie["error"]
        except Exception as e:
            message = str(e)
        await say(ctx, message)

    @command(scope=scope)
    async def pokeswears(self, ctx: sctx) -> None:
        pass

    subcommand = pokeswears.subcommand

    @subcommand()
    @option(description="The word to add to the block list.", required=True)
    async def blocked_add(self, ctx: sctx, word: str) -> None:
        """Add a word to the poke-swears block list."""
        setting: Setting = self.settings["pokeswears_blocklist"]
        block_list: list[str] = words if (words := await setting.get(
            ctx.guild_id)) else []
        if word not in block_list:
            block_list.append(word)
        await setting.set(ctx.guild_id, block_list)
        await say(ctx, f"Added {word} to the block list.")

    @subcommand()
    @option(description="The word to remove from the block list.",
            required=True)
    async def blocked_remove(self, ctx: sctx, word: str) -> None:
        """Remove a word from the poke-swears block list."""
        setting: Setting = self.settings["pokeswears_blocklist"]
        block_list: list[str] = words if (words := await setting.get(
            ctx.guild_id)) else []
        block_list.remove(word)
        await setting.set(ctx.guild_id, block_list)
        await say(ctx, f"Removed {word} from the block list.")

    @subcommand()
    async def blocked_clear(self, ctx: sctx) -> None:
        """Clear the poke-swears block list."""
        setting: Setting = self.settings["pokeswears_blocklist"]
        await setting.delete(ctx.guild_id)
        await say(ctx,
                  "Cleared the block list, will go back to blocking swears.")

    @subcommand()
    @option(description="The word to add to the sub list.", required=True)
    async def subbed_add(self, ctx: sctx, word: str) -> None:
        """Add a word to the poke-swears sub list."""
        setting: Setting = self.settings["pokeswears_sublist"]
        sub_list: list[str] = words if (words := await setting.get(ctx.guild_id
                                                                   )) else []
        if word not in sub_list:
            sub_list.append(word)
        await setting.set(ctx.guild_id, sub_list)
        await say(ctx, f"Added {word} to the sub list.")

    @subcommand()
    @option(description="The word to remove from the sub list.", required=True)
    async def subbed_remove(self, ctx: sctx, word: str) -> None:
        """Remove a word from the poke-swears sub list."""
        setting: Setting = self.settings["pokeswears_sublist"]
        sub_list: list[str] = words if (words := await setting.get(ctx.guild_id
                                                                   )) else []
        sub_list.remove(word)
        await setting.set(ctx.guild_id, sub_list)
        await say(ctx, f"Removed {word} from the sub list.")

    @subcommand()
    async def subbed_clear(self, ctx: sctx) -> None:
        """Clear the poke-swears sub list."""
        setting: Setting = self.settings["pokeswears_sublist"]
        await setting.delete(ctx.guild_id)
        await say(ctx,
                  "Cleared the sub list, will go back to subbing in Pokemon.")

    @subcommand()
    @option(description="The word to replace.", required=True)
    @option(description="The word to substitute in.", required=True)
    async def replacement_add(self, ctx: sctx, word_out: str,
                              word_in: str) -> None:
        """Add a pair to the poke-swears replace dict."""
        setting: Setting = self.settings["pokeswears_replacedict"]
        replace_dict: dict[str, str] = replacements if (
            replacements := await setting.get(ctx.guild_id)) else {}
        replace_dict[word_out] = word_in
        await setting.set(ctx.guild_id, replace_dict)
        await say(ctx, f"{word_out} will be replaced with {word_in}.")

    @subcommand()
    @option(description="The word to remove from the replace dict.",
            required=True)
    async def replacement_remove(self, ctx: sctx, word: str) -> None:
        """Remove a pair from the poke-swears replace dict."""
        setting: Setting = self.settings["pokeswears_replacedict"]
        replace_dict: dict[str, str] = replacements if (
            replacements := await setting.get(ctx.guild_id)) else {}
        del replace_dict[word]
        await setting.set(ctx.guild_id, replace_dict)
        await say(ctx, f"Removed {word} from the replace dict.")

    @subcommand()
    async def replacement_show(self, ctx: sctx) -> None:
        """Show the poke-swears replace dict"""
        setting: Setting = self.settings["pokeswears_replacedict"]
        replace_dict: dict[str, str] = replacements if (
            replacements := await setting.get(ctx.guild_id)) else {}
        await say(ctx, str(replace_dict))

    @subcommand()
    async def replacement_clear(self, ctx: sctx) -> None:
        """Clear the poke-swears replace dict."""
        setting: Setting = self.settings["pokeswears_replacedict"]
        await setting.delete(ctx.guild_id)
        await say(ctx, "Cleared the replace dict.")

    @listener(name="on_message_create")  # type: ignore
    async def poke_swears(self, message: Message, edit: bool = False) -> None:
        """A swear word filter.

        Poke-swears parses message content for words in the swears list, and
        re-posts them with attribution with all the swears replaced with random
        Pokemon names.
        """
        assert isinstance(self.bot._http, HTTPClient)
        message._client = self.bot._http

        blocklist_setting: Setting = self.settings["pokeswears_blocklist"]
        sublist_setting: Setting = self.settings["pokeswears_sublist"]
        replacement_setting: Setting = self.settings["pokeswears_replacedict"]

        blocklist: list[str] = blocks if (
            blocks := await blocklist_setting.get(message.guild_id
                                                  )) else self.swears
        sublist: list[str] = subs if (subs := await sublist_setting.get(
            message.guild_id)) else self.pokemon
        replacedict: dict[str, str] = reps if (
            reps := await replacement_setting.get(message.guild_id)) else {}

        def replace(match: re.Match) -> str:
            word: str = match.group()
            if word.lower() in replacedict:
                return replacedict[word]
            if word.lower() in blocklist:
                return random.choice(list(sublist)).upper()
            return word

        if await self.settings["pokeswears"](message.guild_id):
            if message.content and message.member:
                text = re.sub(r"\b\w*\b", replace, message.content, flags=re.I)
                if message.content != text:
                    await message.delete()
                    user: User = message.author
                    author: Member = message.member
                    author_name: str = f"{author.nick} ({user.username})" if author.nick else user.username
                    embed = Embed(
                        description=text,
                        author=EmbedAuthor(name=author_name,
                                           icon_url=user.avatar_url),
                        footer=EmbedFooter(text="(edit)") if edit else None)
                    channel: Channel = await message.get_channel()
                    await channel.send(embeds=embed)

    @listener(name="on_message_update")  # type: ignore
    async def poke_swears_update(self, before: Message | None,
                                 after: Message) -> None:
        """Simply calls poke_swears after a message edit."""
        assert isinstance(self.bot._http, HTTPClient)

        after._client = self.bot._http
        await self.poke_swears(after, edit=True)

    @listener(name="on_message_create")  # type: ignore
    async def easter_eggs(self, message: Message) -> None:
        """A meme listener to reply to invalid command invokes.

        If a message looks a command invoke, and it doesn't match any commands
        in the commands list, check the users dictionary for an entry to say.
        Fails silently if no keys are found in the users dictionary.
        """
        content: str = message.content
        if content.startswith("/") and message.guild_id:
            commands: list[str] = await get_command_list(
                self.bot, int(message.guild_id), True)
            first_word: str = content[1:].partition(" ")[0].lower()
            if first_word not in commands:
                user_doc = await service.get(str(message.guild_id),
                                             "user_dict", first_word)
                if user_doc is not None:
                    embed: Embed = Embed(description=user_doc,
                                         title=first_word)
                    channel: Channel = await message.get_channel()
                    await channel.send(embeds=embed)


def setup(bot: Client):
    Meme(bot)
