# Noodle is an open-source and modular Discord moderation bot.
# Copyright (C) 2022-2023 Deadspike-san
"""Reaction-based bot interactions."""

from interactions import (
    Client,
    Extension,
    Member,
    MessageReaction,
    Emoji,
    Role,
    extension_command as base,
    extension_listener as listener,
)


class Reactions(Extension):

    def __init__(self, bot: Client) -> None:
        self.bot = bot

    @listener(name="on_message_reaction_add")  # type: ignore
    async def reaction_role(self, message: MessageReaction) -> None:
        """Grant a role on reaction to a specified message."""

        if str(message.message_id) == "1091041868772556952" and str(
                message.emoji.id) == "1091039956719378578":
            role: Role | None = await message.member.guild.get_role(
                1091049969265414214)
            if role:
                await message.member.add_role(role)


def setup(bot: Client):
    Reactions(bot)
